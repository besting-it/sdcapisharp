﻿using System;
using System.Collections;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SdcApiSharp
{
    public enum EventType
    {
        Episodic,
        Periodic
    }
    
    public class EventCallback<TReturn> : IDisposable
    {
        public EventHandler<TReturn> ResultReceived;
        public EventHandler<Exception> InvokeFailing;
        private CancellationTokenSource cts;
        private TReturn last;
        
        public void RegisterRemoteCall(Func<Task<TReturn>> call, EventType type = EventType.Episodic, int delayMs = 1000)
        {
            if (cts != null)
                throw new InvalidOperationException("Callback already in use!");
            cts = new CancellationTokenSource();

            async void StartPolling()
            {
                try
                {
                    while (true)
                    {
                        cts.Token.ThrowIfCancellationRequested();
                        var res = await call();
                        if (type == EventType.Periodic || last == null || !IsEqual(res, last))
                        {
                            ResultReceived?.Invoke(this, res);
                        }
                        last = res;
                        await Task.Delay(delayMs, cts.Token);
                    }
                }
                catch (OperationCanceledException)
                {
                }
                catch (Exception e)
                {
                    InvokeFailing?.Invoke(this, e);
                }                
            }

            StartPolling();
        }

        private bool IsEqual(object obj1, object obj2)
        {
            if (obj1 is IEnumerable enum1 && obj2 is IEnumerable enum2)
            {
                var en1 = enum1.GetEnumerator();
                var en2 = enum2.GetEnumerator();
                while (en1.MoveNext() && en2.MoveNext())
                    if (!en1.Current.Equals((en2.Current)))
                        return false;
                return true;
            }
            return obj1.Equals(obj2);
        }

        public void Dispose()
        {
            cts?.Cancel();
            cts?.Dispose();
            cts = null;
        }
    }
}