﻿using System;
using System.Collections.Generic;
using SdcApiSharp.Api;
using SdcApiSharp.Client;
using SdcApi.Model;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SdcApiSharp.Model;

namespace SdcApiSharp
{
    class Program
    {
        
        static async Task Main(string[] args)
        {
            Configuration.Default.BasePath = "http://localhost:8064/sdcapi/v1";
            var api = new DefaultApi(Configuration.Default);

            try
            {
                // Get a list of endpoint references (currently active SDC providers)
                var providers = await api.GetSdcProvidersAsync();
                Debug.WriteLine("Currently active providers: " + string.Join(", ", providers));

                // Get first provider found
                var epr = providers.FirstOrDefault();
                if (epr == null)
                {
                    Debug.WriteLine("No provider found.");
                    return;
                }

                Debug.WriteLine("Found provider, EPR: " + epr);

                // Get the MdDescription
                var descrString = await api.GetDescriptionAsync(epr);
                MdDescription mdd = MdDescription.Deserialize(descrString);
                MdsDescriptorMetaData meta = mdd.Mds.First().MetaData;
                var modelName = meta.ModelName.FirstOrDefault();
                if (modelName != null)
                    Debug.WriteLine("Found model name: " + modelName.Value);

                // Get and deserialize MdState container
                var mdStateString = await api.GetStatesAsync(epr);
                var states = MdState.Deserialize(mdStateString).State;
                var handles = string.Join(", ", states.Select(s => s.DescriptorHandle));
                Debug.WriteLine("Found state with handles: " + handles);

                // Get first numeric metric state
                NumericMetricState nms = states.OfType<NumericMetricState>().FirstOrDefault();
                if (nms == null)
                {
                    Debug.WriteLine("No metric state found.");
                    return;
                }

                // Get numeric value
                NumericMetricValue value = nms.MetricValue;
                Debug.WriteLine("Current value of first metric state: " + value.Value);
                
                // Change value, serialize and set state
                value.Value = 1.23m;
                Debug.WriteLine("Trying to commit state with value: " + value.Value);

                // Cast to AbstractState and serialize
                string body = (nms as AbstractState).Serialize();
                InvocationResult res = await api.SetStateAsync(epr, Encoding.ASCII.GetBytes(body));
                Debug.WriteLine("Direct response: " + res.InvocationState);

                var tcs = new TaskCompletionSource<bool>();
                
                // Register episodic event reception of invocation results
                var callBack = new EventCallback<List<InvocationState>>();
                callBack.ResultReceived += (s, invStates) =>
                {
                    Debug.WriteLine("Received invocation states: " + string.Join(", ", invStates));
                    // Set complete on entry "Fin"
                    if (invStates.Contains(InvocationState.Fin) || invStates.Contains(InvocationState.FinMod))
                        tcs.SetResult(true);
                };
                callBack.InvokeFailing += (s, exc) =>
                {
                    Debug.WriteLine("Error on event invoke: " + exc.Message);
                };                
                // Provide function call to event handler
                callBack.RegisterRemoteCall(async () => await api.GetInvocationStatesAsync(epr, res.TransactionId), 
                    EventType.Episodic);

                await Task.WhenAny(tcs.Task, Task.Delay(3000));
                if (tcs.Task.IsCompleted)
                    Debug.WriteLine("State modified.");
                else
                    Debug.WriteLine("State not modified.");

                // Set / get value using simple blocking method
                double testSet = 3.14;
                await api.SetValueAsync(epr, nms.DescriptorHandle, testSet.ToString(CultureInfo.InvariantCulture), 5000);
                var testGet = await api.GetValueAsync(epr, nms.DescriptorHandle);
                double diff = Math.Abs(double.Parse(testGet, CultureInfo.InvariantCulture) - testSet);
                if (diff < 0.001)
                {
                    Debug.WriteLine("Value modified to:" + testSet);
                }
                callBack.Dispose();
            }
            catch (ApiException e)
            {
                Debug.WriteLine("Exception when calling Api: " + e.Message);
                Debug.WriteLine("Status Code: " + e.ErrorCode);
                Debug.WriteLine(e.StackTrace);
            }


        }
    }
}
