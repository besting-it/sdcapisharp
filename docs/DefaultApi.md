# SdcApiSharp.Api.DefaultApi

All URIs are relative to *http://sdcapi/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetDescription**](DefaultApi.md#getdescription) | **GET** /provider/{epr}/get/description | Get the static description of an SDC provider.
[**GetInvocationStates**](DefaultApi.md#getinvocationstates) | **GET** /provider/get/invocationstates/{epr}/{id} | Get a list of invocation states.
[**GetSdcProviders**](DefaultApi.md#getsdcproviders) | **GET** /discovery | Get a list of currently active SDC providers.
[**GetStates**](DefaultApi.md#getstates) | **GET** /provider/{epr}/get/states | Get a list of SDC provider states.
[**GetValue**](DefaultApi.md#getvalue) | **GET** /provider/{epr}/get/value/{handle} | Get a single state value
[**SetState**](DefaultApi.md#setstate) | **POST** /provider/{epr}/set/state | Set a state on an SDC provider
[**SetValue**](DefaultApi.md#setvalue) | **POST** /provider/{epr}/set/value/{handle} | Set a single state value



## GetDescription

> string GetDescription (string epr)

Get the static description of an SDC provider.

Returns an MdDescription object containing the provider description.

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using SdcApiSharp.Api;
using SdcApiSharp.Client;
using SdcApiSharp.Model;

namespace Example
{
    public class GetDescriptionExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://sdcapi/v1";
            var apiInstance = new DefaultApi(Configuration.Default);
            var epr = epr_example;  // string | The endpoint reference of the SDC provider

            try
            {
                // Get the static description of an SDC provider.
                string result = apiInstance.GetDescription(epr);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling DefaultApi.GetDescription: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **string**| The endpoint reference of the SDC provider | 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/xml

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **464** | Invalid EPR, provider not found |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetInvocationStates

> List&lt;InvocationState&gt; GetInvocationStates (string epr, long id)

Get a list of invocation states.

Returns a list of invocation states for a current transaction.

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using SdcApiSharp.Api;
using SdcApiSharp.Client;
using SdcApiSharp.Model;

namespace Example
{
    public class GetInvocationStatesExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://sdcapi/v1";
            var apiInstance = new DefaultApi(Configuration.Default);
            var epr = epr_example;  // string | The endpoint reference of the SDC provider
            var id = 789;  // long | The transaction id

            try
            {
                // Get a list of invocation states.
                List<InvocationState> result = apiInstance.GetInvocationStates(epr, id);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling DefaultApi.GetInvocationStates: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **string**| The endpoint reference of the SDC provider | 
 **id** | **long**| The transaction id | 

### Return type

[**List&lt;InvocationState&gt;**](InvocationState.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **400** | Bad parameter provided |  -  |
| **464** | Invalid transaction id |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetSdcProviders

> List&lt;string&gt; GetSdcProviders ()

Get a list of currently active SDC providers.

Returns a list containing endpoint references of SDC providers available in the SDC network.

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using SdcApiSharp.Api;
using SdcApiSharp.Client;
using SdcApiSharp.Model;

namespace Example
{
    public class GetSdcProvidersExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://sdcapi/v1";
            var apiInstance = new DefaultApi(Configuration.Default);

            try
            {
                // Get a list of currently active SDC providers.
                List<string> result = apiInstance.GetSdcProviders();
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling DefaultApi.GetSdcProviders: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

**List<string>**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetStates

> string GetStates (string epr, string handle = null)

Get a list of SDC provider states.

Returns an MdState object containing states

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using SdcApiSharp.Api;
using SdcApiSharp.Client;
using SdcApiSharp.Model;

namespace Example
{
    public class GetStatesExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://sdcapi/v1";
            var apiInstance = new DefaultApi(Configuration.Default);
            var epr = epr_example;  // string | The endpoint reference of the SDC provider
            var handle = handle_example;  // string | An optional filter handle to retrieve a single state (optional) 

            try
            {
                // Get a list of SDC provider states.
                string result = apiInstance.GetStates(epr, handle);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling DefaultApi.GetStates: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **string**| The endpoint reference of the SDC provider | 
 **handle** | **string**| An optional filter handle to retrieve a single state | [optional] 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/xml

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **464** | Invalid EPR, provider not found |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetValue

> string GetValue (string epr, string handle)

Get a single state value

Returns a value of a state

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using SdcApiSharp.Api;
using SdcApiSharp.Client;
using SdcApiSharp.Model;

namespace Example
{
    public class GetValueExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://sdcapi/v1";
            var apiInstance = new DefaultApi(Configuration.Default);
            var epr = epr_example;  // string | The endpoint reference of the SDC provider
            var handle = handle_example;  // string | The descriptor handle of the state

            try
            {
                // Get a single state value
                string result = apiInstance.GetValue(epr, handle);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling DefaultApi.GetValue: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **string**| The endpoint reference of the SDC provider | 
 **handle** | **string**| The descriptor handle of the state | 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: text/plain

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **464** | Invalid EPR, provider not found |  -  |
| **465** | Invalid handle, state not found |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetState

> InvocationResult SetState (string epr, byte[] body)

Set a state on an SDC provider

Set a specific AbstractState object on the provider

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using SdcApiSharp.Api;
using SdcApiSharp.Client;
using SdcApiSharp.Model;

namespace Example
{
    public class SetStateExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://sdcapi/v1";
            var apiInstance = new DefaultApi(Configuration.Default);
            var epr = epr_example;  // string | The endpoint reference of the SDC provider
            var body = BYTE_ARRAY_DATA_HERE;  // byte[] | Xml string representation of the state to set

            try
            {
                // Set a state on an SDC provider
                InvocationResult result = apiInstance.SetState(epr, body);
                Debug.WriteLine(result);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling DefaultApi.SetState: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **string**| The endpoint reference of the SDC provider | 
 **body** | **byte[]**| Xml string representation of the state to set | 

### Return type

[**InvocationResult**](InvocationResult.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/xml
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **400** | Bad data provided |  -  |
| **464** | Invalid EPR, provider not found |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SetValue

> void SetValue (string epr, string handle, string value, int? timeout = null)

Set a single state value

Sets a value of a state. This method blocks until the value has been set or an error occurred.

### Example

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using SdcApiSharp.Api;
using SdcApiSharp.Client;
using SdcApiSharp.Model;

namespace Example
{
    public class SetValueExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "http://sdcapi/v1";
            var apiInstance = new DefaultApi(Configuration.Default);
            var epr = epr_example;  // string | The endpoint reference of the SDC provider
            var handle = handle_example;  // string | The descriptor handle of the state
            var value = value_example;  // string | The value of the state to set
            var timeout = 56;  // int? | Timeout in milliseconds to wait until state is modified (optional) 

            try
            {
                // Set a single state value
                apiInstance.SetValue(epr, handle, value, timeout);
            }
            catch (ApiException e)
            {
                Debug.Print("Exception when calling DefaultApi.SetValue: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **epr** | **string**| The endpoint reference of the SDC provider | 
 **handle** | **string**| The descriptor handle of the state | 
 **value** | **string**| The value of the state to set | 
 **timeout** | **int?**| Timeout in milliseconds to wait until state is modified | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Successful operation |  -  |
| **400** | Bad data provided |  -  |
| **464** | Invalid EPR, provider not found |  -  |
| **465** | Error modifying state |  -  |

[[Back to top]](#)
[[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

