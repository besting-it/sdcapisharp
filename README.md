# SdcApiSharp - C# library for the IEEE 11073 SDC Family REST API

This is a C# client for accessing SDC REST API functions of exisiting frameworks supporting the SDC REST API.

- [SdcApiSpec](https://bitbucket.org/besting-it/sdcapispec) - SDC REST API specification (can be used for generating client and server stubs)

This client requires a server side endpoint such as SDCLib/J in middleware mode. See the link above for more information.

Licensed under the Apache License, Version 2.0.

## Frameworks supported

- .NET 4.0 or later

## Dependencies

- [RestSharp](https://www.nuget.org/packages/RestSharp) - 105.1.0 or later
- [Json.NET](https://www.nuget.org/packages/Newtonsoft.Json/) - 7.0.0 or later
- [JsonSubTypes](https://www.nuget.org/packages/JsonSubTypes/) - 1.2.0 or later

## Nuget link

A ready-to-use nuget package is available here:

- [SdcApiSharp](https://www.nuget.org/packages/SdcApiSharp) - SdcApiSharp nuget

## Documentation for API Endpoints

All URIs are relative to http://\[localhost/\]sdcapi/v

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**GetDescription**](docs/DefaultApi.md#getdescription) | **GET** /provider/{epr}/get/description | Get the static description of an SDC provider.
*DefaultApi* | [**GetInvocationStates**](docs/DefaultApi.md#getinvocationstates) | **GET** /provider/get/invocationstates/{epr}/{id} | Get a list of invocation states.
*DefaultApi* | [**GetSdcProviders**](docs/DefaultApi.md#getsdcproviders) | **GET** /discovery | Get a list of currently active SDC providers.
*DefaultApi* | [**GetStates**](docs/DefaultApi.md#getstates) | **GET** /provider/{epr}/get/states | Get a list of SDC provider states.
*DefaultApi* | [**GetValue**](docs/DefaultApi.md#getvalue) | **GET** /provider/{epr}/get/value/{handle} | Get a single state value
*DefaultApi* | [**SetState**](docs/DefaultApi.md#setstate) | **POST** /provider/{epr}/set/state | Set a state on an SDC provider
*DefaultApi* | [**SetValue**](docs/DefaultApi.md#setvalue) | **POST** /provider/{epr}/set/value/{handle} | Set a single state value


## Documentation for Models

 - [Model.InvocationResult](docs/InvocationResult.md)
 - [Model.InvocationState](docs/InvocationState.md)


## Documentation for Authorization

All endpoints do not require authorization.

## Getting Started

Some examples for using the SDC API.

### Discovery

```csharp
static async Task Main(string[] args)
{
    Configuration.Default.BasePath = "http://localhost:8064/sdcapi/v1";
    var api = new DefaultApi(Configuration.Default);

    try
    {
        // Get a list of endpoint references (currently active SDC providers)
        var providers = await api.GetSdcProvidersAsync();
        Debug.WriteLine("Currently active providers: " + string.Join(", ", providers));

        // Get first provider found
        var epr = providers.FirstOrDefault();
        if (epr == null)
        {
            Debug.WriteLine("No provider found.");
            return;
        }

        Debug.WriteLine("Found provider, EPR: " + epr);
    }
    catch (ApiException e)
    {
        Debug.WriteLine("Exception when calling Api: " + e.Message);
        Debug.WriteLine("Status Code: " + e.ErrorCode);
        Debug.WriteLine(e.StackTrace);
    }
}
```

In the following examples the try-catch block is omitted.

### Getting the MdDescription and MdState container

```csharp
// Get the MdDescription
var descrString = await api.GetDescriptionAsync(epr);
MdDescription mdd = MdDescription.Deserialize(descrString);
MdsDescriptorMetaData meta = mdd.Mds.First().MetaData;
var modelName = meta.ModelName.FirstOrDefault();
if (modelName != null)
    Debug.WriteLine("Found model name: " + modelName.Value);

// Get and deserialize MdState container
var mdStateString = await api.GetStatesAsync(epr);
var states = MdState.Deserialize(mdStateString).State;
var handles = string.Join(", ", states.Select(s => s.DescriptorHandle));
Debug.WriteLine("Found state with handles: " + handles);

// Get first numeric metric state
NumericMetricState nms = states.OfType<NumericMetricState>().FirstOrDefault();
if (nms == null)
{
    Debug.WriteLine("No metric state found.");
    return;
}
```     

### Getting the numeric value from the state, change it and commit the change

Note that this example also shows how to use the event driven extension for receiving episodic invocation states.
The event mechanism can be used in conjuction with any method available.

```csharp
// Get numeric value
NumericMetricValue value = nms.MetricValue;
Debug.WriteLine("Current value of first metric state: " + value.Value);

// Change value, serialize and set state
value.Value = 1.23m;
Debug.WriteLine("Trying to commit state with value: " + value.Value);

// Cast to AbstractState and serialize
string body = (nms as AbstractState).Serialize();
InvocationResult res = await api.SetStateAsync(epr, Encoding.ASCII.GetBytes(body));
Debug.WriteLine("Direct response: " + res.InvocationState);

// Create task completion source for event driven waiting
var tcs = new TaskCompletionSource<bool>();

// Register episodic event reception of invocation results
using (var callBack = new EventCallback<List<InvocationState>>())
{
	callBack.ResultReceived += (s, invStates) =>
	{
		Debug.WriteLine("Received invocation states: " + string.Join(", ", invStates));
		// Set complete on entry "Fin"
		if (invStates.Contains(InvocationState.Fin) || invStates.Contains(InvocationState.FinMod))
			tcs.SetResult(true);
	};
	callBack.InvokeFailing += (s, exc) =>
	{
		Debug.WriteLine("Error on event invoke: " + exc.Message);
	};                

	// Provide function call to event handler
	callBack.RegisterRemoteCall(async () => await api.GetInvocationStatesAsync(epr, res.TransactionId), 
		EventType.Episodic);

	await Task.WhenAny(tcs.Task, Task.Delay(3000));
	if (tcs.Task.IsCompleted)
		Debug.WriteLine("State modified.");
	else
		Debug.WriteLine("State not modified in time.");
}
```

### Using a more simpler method for just setting the numeric value of a state

Note that in this method the function called to change a value blocks until finished.

```csharp
// Set / get value using simple blocking method
double testSet = 3.14;
await api.SetValueAsync(epr, nms.DescriptorHandle, testSet.ToString(CultureInfo.InvariantCulture), 5000);
var testGet = await api.GetValueAsync(epr, nms.DescriptorHandle);
double diff = Math.Abs(double.Parse(testGet, CultureInfo.InvariantCulture) - testSet);
if (diff < 0.001)
{
    Debug.WriteLine("Value modified to: " + testSet);
}		
```

### Example output for the examples shown before

```
Currently active providers: UDI-1234567890
Found provider, EPR: UDI-1234567890
Found model name: SDCLib Demo device
Found state with handles: handle_mds, handle_stream, handle_metric
Current value of first metric state: 1.00
Trying to commit state with value: 1.23
Direct response: Wait
Received invocation states: Wait, Start, Fin
State modified.
Value modified to: 3.14
```

## Changelog

* Version 1.2.0 2020/09/06 Added get/set value
* Version 1.1.0 2020/08/30 Added event helpers
* Version 1.0.0 2020/05/28 Initial release

## Misc

This library was partially built using the [OpenAPI Generator](https://openapi-generator.tech) project.
